# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [0.5.2] - 2021-01-19
### Fix
- Wrong case structure in pressing shift.

## [0.5.1] - 2021-01-08
### Fix
- Constant Documentation Test ignores errors on Ctls.

## [0.5.0] - 2020-12-22
### Added
- VI for analysing failing VI's tests rank.
### Changed
- New words added in VI Analyzer User Dictionary

## [0.4.3] - 2020-11-04
### Changed
- New words added in VI Analyzer User Dictionary

## [0.4.2] - 2020-10-21
### Changed
- New words added in VI Analyzer User Dictionary

## [0.4.1] - 2020-10-07
### Changed
- Included .Ctl in the changed files filter.
- New words added in VI Analyzer User Dictionary

## [0.4.0] - 2020-09-18
### Changed
- New words added in VI Analyzer User Dictionary
- Changed VI Analyzer Quick Drop test target, description modified in README.

## [0.3.0] - 2020-09-18
### Changed
- New words added in VI Analyzer User Dictionary
### Fixed
- Custom VI Analyzer for Constant Documentation was reporting errors on Ctls.

## [0.2.1] - 2020-08-25
### Changed
- New words added in VI Analyzer User Dictionary: Unsubscribe, unsubscribe

## [0.2.0] - 2020-08-22
#### Added
- Custom Tests for VI Analyer and a Example Complete Test.

### Changed
- QD VI Analyer Plugin. With shift it calls now the complete test routine.

## [0.1.3] - 2020-08-18
### Changed
- QD Plugin for opening git-bash, changed the path to call directly the app (Path Environment)

## [0.1.2] - 2020-07-14
### Added
- New words added in- VI Analyzer User Dictionary: Carlos, Henrique, Szollosi.

## [0.1.1] - 2020-07-14
### Changed
- Updated Git Project Template with new vendor folder.

## [0.1.0] - 2020-07-13
### Added
- Initial Version

# Common Plugins
Common plugins for LV Daily Use and Project Standardization

# Usage
- Clone this repository into your LabVIEW Data with the command.

`git init && git symbolic-ref HEAD refs/heads/main && git remote add origin https://gitlab.com/felipefoz/common-plugins.git && git fetch --all && git reset --hard origin/main`

# Author
- Felipe Pinheiro Silva

# Project Templates
- Custom Project - Template for Git Projects

# VI Analyzer
- User Dictionary
- ConfigFile1.viancfg
- ConfigFile2.viancfg

# VI Analyzer Custom Tests
- [CaseSensitiveCaseStructure.llb](https://forums.ni.com/docs/DOC-29189)
- [CheckLVLibUsage.llb](https://forums.ni.com/docs/DOC-41432)
- [CheckWhitespaceInFPnames.llb](https://forums.ni.com/docs/DOC-29191)
- [Constant Documentation.llb](https://forums.ni.com/docs/DOC-35487)
- O[pen Actor Core Front Panel Set to True.llb](https://forums.ni.com/docs/DOC-48110)
- [Separated Compiled Code.llb](https://forums.ni.com/docs/DOC-29170) - Included in LV2020
- [Useless Close Reference.llb](https://forums.ni.com/t5/VI-Analyzer-Enthusiasts/Test-Useless-Close-Reference/ta-p/3743283)

# Quick Drop Plugins
- [Nattify - Ctrl + N](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Quick-Drop-Keyboard-Shortcut-Nattify-VI/gpm-p/3990402)
  - Using "p" avoid FP Arrangement and Color Change;
- [Numeric Representation - Ctrl + Y](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Setting-Numeric-Representation/gpm-p/3507868)
- Wire Bends Remove - Ctrl + X
  - Experimental for failing test in VI Analyzer. It may be incorporated into Nattify.
- [Show VI in Folder - Ctrl + E](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Show-VI-in-Windows-Explorer/gpm-p/3490730)
- [Clear Array - Ctrl +C](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Quick-Drop-Keyboard-Shortcut-Clear-Array/gpm-p/3529314)
- Git Bash in Folder - Ctrl + G
  - Opens Git Bash in Project Folder or with Shift opens in VI Folder.
- [VI Analyzer Git Changed Files - Ctrl + V](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/VI-Analyzer-Git-Changed-Files/gpm-p/4065934)
  - Ctrl + V - Runs the project test (test/static) only on git changed files.
  - Ctrl + Shift + V  - Runs the project test (test/static) according to its configuration.

# Shortcut Menu Plugins (Right-Click Menu)
- [Add Custom Bookmark](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Add-Bookmark-llb/ta-p/3613618)
- [Add to Class Data](https://github.com/TomsLabVIEWExtensions/Add-data-to-class/blob/master/Add%20to%20class.llb)
- [Add Value Change Event](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Add-Value-Change-Event-llb/ta-p/3908644)
- [Assign to Connector Pane](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Assign-to-Connector-Pane-llb/ta-p/4053922)
- [Change to bundle or unbundle](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Change-To-Bundle-Or-Unbundle-llb/tac-p/4045629)
- [Copy Delimited Data](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Copy-Delimited-Data-llb/ta-p/3538576)
- [Create IPES Border Node](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Create-IPES-Border-Node-llb/ta-p/3712355)
- [Expand Cluster Constant](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Expand-Cluster-Constant-llb/ta-p/3517818)
- [Find Events](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Find-Events-llb-for-ctrl-ind-FPTerm-local-var-ctrl-ref/ta-p/3518130)
- [Grow to N](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Grow-to-n-llb/ta-p/3523765)
- [Hide or Show FP Elements](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Hide-or-Show-FP-Controls-llb/ta-p/4045090)
- [Insert IPE in Any Wire](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Insert-IPE-in-Any-Wire-AZ-llb/ta-p/4012959)
- [Move up or Down Bundle Element](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Move-unbundle-item-llb/ta-p/4015889)
- [Multi-select change variable](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Multi-select-Change-Local-Direction/ta-p/3526840)
- [Open Message Handler](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Open-Message-Handler-llb/ta-p/3871400)
- [Set Current Event to Value Change](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Set-Current-Event-to-Value-Change-llb/ta-p/3538838)
- [Wire Multiple Items to Bundler](https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/Wire-Multiple-Items-to-Bundler-llb/ta-p/3517939)
